import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthorizeComponent } from "./modules/authorize/pages/authorize.component";


const routes: Routes = [
  {
    path: "",
    redirectTo: "/topfilms",
    pathMatch: "full"
  },
  {
    path: "topfilms",
    loadChildren: "./modules/movies/movies.module#MoviesModule"
  },
  {
    path: "topshows",
    loadChildren: "./modules/shows/shows.module#ShowsModule"
  },
  {
    path: "watched",
    loadChildren:
      "./modules/watched-medias/watched-medias.module#WatchedMediasModule",
  },
  {
    path: "wishlist",
    loadChildren: "./modules/watchlist/watchlist.module#WatchlistModule"
  },
  { path: "authorize", component: AuthorizeComponent },
  {
    path: "search",
    loadChildren: "./modules/search/search.module#SearchModule"
  },

  {
    path: "actor",
    loadChildren: "./modules/shared/shared.module#SharedModule"
  },

  { path: "**", loadChildren: "./modules/page404/page404.module#Page404Module" }
];

@NgModule({
  // onSameUrl: permet de reloader une page en changeant l'id du composant
  // Utilisé dans les détails des médias pour naviguer vers les détails d'un média similaires sans changer d'URL (uniquement le /id change)
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
