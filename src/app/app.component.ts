import { Component } from "@angular/core";
import { OauthService } from "src/app/modules/authorize/oauth.service";
import { OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "Gamflix";
  isLogged: boolean;

  constructor(private _oauthService: OauthService) {}

  ngOnInit() {
    this._oauthService.isLoggedIn$.subscribe(res => {
      this.isLogged = res;
    });
  }
}
