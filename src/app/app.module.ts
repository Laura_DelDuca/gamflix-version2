import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./modules/shared/components/navbar/navbar.component";
import { AuthorizeComponent } from "./modules/authorize/pages/authorize.component";
import { ConvertToYearsPipe } from "./modules/shared/pages/detailactor/pipes/convert-to-years.pipe";
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AuthorizeComponent,
    ConvertToYearsPipe

  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, SharedModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
