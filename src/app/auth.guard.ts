import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { OauthService } from "./modules/authorize/oauth.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  isLogged: boolean

  constructor(private router: Router, private _oauthService: OauthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    this._oauthService.isLoggedIn$.subscribe(res => this.isLogged = res);
    let url: string = state.url;
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this.isLogged !== true) {
      this.router.navigate(["/topfilms"]);
      return false;
    }

    else { 
    return true;
  }}
}
