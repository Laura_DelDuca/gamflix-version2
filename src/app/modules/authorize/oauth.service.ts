import { Injectable } from "@angular/core";
import { HttpHeaders, HttpSentEvent } from "@angular/common/http";
import { Observable, ObservableInput, BehaviorSubject } from "rxjs";
import { IToken } from "./token";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { TokenLocalStorageService } from "../shared/services/revoke.service";

@Injectable({
  providedIn: "root"
})
export class OauthService {
  isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isLoggedIn$ = this.isLoggedIn.asObservable();
  isLogged: boolean
  // Le lien dans la navbar dirige vers trakt où on doit s'identifier;
  // après identification, on est renvoyé sur notre application avec un code dans l'URL
  // On doit récupérer ce code et l'échanger contre un token
  // Pour cela, on doit récupérer d'autres paramètres, en plus du code de l'URL:
  // clientID, clientSecret, RedirectURI, et type
  private myToken = {
    // Code qui sera renvoyé dans l'URL quand Trakt nous renvoie vers
    // notre application après l'identification
    code: "",
    client_id:
      "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
    client_secret:
      "a55f12b35d7222acc92364c306504cb9a7930fb6258fa895adbd529b83f4f190",
    redirect_uri: `${environment.trakt.redirectUri}/authorize`,
    grant_type: "authorization_code"
  };

  private _revokeToken = {
    token: "",
    client_id:
      "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
    client_secret:
      "a55f12b35d7222acc92364c306504cb9a7930fb6258fa895adbd529b83f4f190"
  };

  constructor(
    private http: HttpClient,
    private tokenLocalStorageService: TokenLocalStorageService
  ) {}

  // Headers to be added each time a call is made to trakt
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
      "trakt-api-version": "2"
    })
  };

  updateLog(boolean) {
    this.isLoggedIn.next(boolean) 
  }

  // Echange le code reçu de trakt par un access token
  getToken(code: string): Observable<IToken> {
    // Grâce à l'URL dans la navbar, il sait récupérer la réponse, et donc le code renvoyé par Trakt
    // cela se fait dans le component Authorize
    // Il attribue ensuite cette valeur à la clé "code" de la variable myToken
    this.myToken.code = code;
    return this.http.post<IToken>(
      `${environment.trakt.baseUrl}/oauth/token`,
      // On ajoute à la requête les paramètres spécifiques et aussi les paramètres habituels pour chaque requête
      this.myToken,
      this.httpOptions
    );
    
  }

  revokeToken() {
    let token = this.tokenLocalStorageService.getAccessToken();
    this._revokeToken.token = token;

    return this.http.post(
      `${environment.trakt.baseUrl}/oauth/revoke`,
      this._revokeToken,
      this.httpOptions
    );
  }



}
