import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { OauthService } from "../oauth.service";
import { IToken } from "../token";
import { LocalstorageService } from "../../shared/services/localstorage.service";

@Component({
  selector: "app-authorize",
  templateUrl: "./authorize.component.html",
  styleUrls: ["./authorize.component.css"]
})
export class AuthorizeComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private _oauthService: OauthService,
    private _localstorageService: LocalstorageService,
    private router: Router
  ) {}

  ngOnInit() {
    // Ici, on vérifie que trakt a bien renvoyé un code dans l'URL; si oui, on est redirigés vers l'application
    this.route.queryParamMap.subscribe((queryParams: ParamMap) => {
      if (!queryParams.has("code")) {
        return this.router.navigate([""]);
      }
      // Subscription au service oauth pour récupérer le code dans l'URL
      // On stocke le token que nous renvoie la méthode dans le service localstorage
      this._oauthService
        .getToken(queryParams.get("code"))
        .subscribe((response: IToken) => {
          this._localstorageService.setItem("token", JSON.stringify(response));
          this._oauthService.updateLog(true);
          if(localStorage.getItem('token') !== null && localStorage.getItem('token') !== undefined) {
            this.router.navigate(["/topfilms"]);
          }
        });
    });

    // setTimeout(() => {
    //   this.router.navigate(["/topfilms"]);
    // }, 2000);
  }
}
