export interface IFilm {
  title: string;
  year: number;
  ids: {
    trakt: number;
    slug: string;
    imdb: string;
    tmdb: number;
  };
  tagline: string;
  overview: string;
  released: string;
  runtime: number;
  country: string;
  updated_at: string;
  trailer: null;
  homepage: string;
  rating: number;
  votes: number;
  comment_count: number;
  language: string;
  available_translations: string;
  genres: string;
  certification: string;
}
