import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TopfilmsComponent, DetailfilmComponent } from "./pages";

const routes: Routes = [
  {
    path: "",
    component: TopfilmsComponent
  },
  {
    path: ":ids.trakt",
    component: DetailfilmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule {}
