import { Component, OnInit, Input } from "@angular/core";
import { FilmsService } from "../../services/films.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { IFilm } from "../../films";
import { IActor } from "../../../shared/actors";
import { ActorsService } from "../../../shared/services/actors.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-detailfilm",
  templateUrl: "./detailfilm.component.html",
  styleUrls: ["./detailfilm.component.css"]
})
export class DetailfilmComponent implements OnInit {
  public filmId: number;
  film: IFilm;
  public similars;
  actors: IActor[];
  people;
  id: number;
  trailer: string;
  films: IFilm[];
  private _traktIds: number[];
  inWishlist: boolean = true;
  seenmovies: IFilm[];
  private _traktSeenIds: number[];
  inWatchedlist: boolean = true;

  constructor(
    private _filmsService: FilmsService,
    private route: ActivatedRoute,
    private _actorsService: ActorsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Allows to pass the route parameter: the trakt id
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get("ids.trakt"));
      this.filmId = id;
      this.trailer = null;
      this.getFilmsbyId(this.filmId);
      this.getRelatedMovies(id);
      this.getCastingMovie(id);
      this.getWishMovies();
      this.getSeenMovies();
    });
  }

  getFilmsbyId(id): void {
    this._filmsService.getFilmsbyId(this.filmId).subscribe(film => {
      this.film = film;
      this.trailer = this.film.trailer;
    });
  }

  addWishMovie(film: IFilm) {
    this._filmsService.addWishMovie(film).subscribe(() => this.getWishMovies());
  }

  addWatchedMovie(film: IFilm) {
    this._filmsService
      .addWatchedMovie(film)
      .subscribe(() => this.getSeenMovies());
  }

  getRelatedMovies(id) {
    this._filmsService.getRelatedMovies(id).subscribe(similar => {
      this.similars = similar;
    });
  }

  selectFilm(film) {
    // console.log(film);
    this.router.navigate(["/topfilms", film.ids.trakt]);
  }

  getCastingMovie(id) {
    this._actorsService.getCastingMovie(id).subscribe(actor => {
      this.people = actor;
      this.actors = this.people.cast;
    });
  }

  selectActor(actor) {
    this.router.navigate(["/actor", actor.ids.trakt]);
  }

  getWishMovies() {
    this._filmsService.getWishMovies().subscribe(data => {
      this.films = data;
      // console.log(this.films);
      this._traktIds = this.films.map((item: any) => item.movie.ids.trakt);
      this.isMovieIn();
    });
  }

  isMovieIn() {
    this.inWishlist = this._traktIds.includes(this.filmId);
  }

  getSeenMovies() {
    this._filmsService.getSeenMovies().subscribe(data => {
      this.seenmovies = data;
      console.log(this.seenmovies);
      this._traktSeenIds = this.seenmovies.map(
        (item: any) => item.movie.ids.trakt
      );
      console.log(this._traktSeenIds);
      this.isMovieSeen();
    });
  }

  isMovieSeen() {
    this.inWatchedlist = this._traktSeenIds.includes(this.filmId);
    console.log(this.inWatchedlist);
  }
}
