import { TopfilmsComponent } from "./topfilms/topfilms.component";
import { DetailfilmComponent } from "./detailfilm/detailfilm.component";

export const pages: any[] = [TopfilmsComponent, DetailfilmComponent];

export * from "./topfilms/topfilms.component";
export * from "./detailfilm/detailfilm.component";
