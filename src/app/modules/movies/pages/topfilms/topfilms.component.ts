import { Component, OnInit } from "@angular/core";
import { FilmsService } from "../../services/films.service";
import { Router } from "@angular/router";
import { IFilm } from "../../films";
import { OauthService } from 'src/app/modules/authorize/oauth.service';

@Component({
  selector: "app-topfilms",
  templateUrl: "./topfilms.component.html",
  styleUrls: ["./topfilms.component.css"]
})
export class TopfilmsComponent implements OnInit {
  films: IFilm[] = [];
  images;

  currentPage = 1;
  totalPage: number;
  hasNext = true;
  // hasPrevious = true;
  limit = 10;

  constructor(private _filmsService: FilmsService, private router: Router, private _oauthService: OauthService) {}

  ngOnInit() {
    this.getPopularFilms();
  }

  getPopularFilms() {
    this._filmsService
      .getPopularFilms(this.currentPage, this.limit)
      .subscribe(response => {
        this.totalPage = response.totalPage;
        this.films = [...this.films, ...response.movies];

        this.hasNext = response.currentPage < response.totalPageCount;
        //this.hasPrevious = response.currentPage > 1;
      });
  }

  onSelect(film) {
    this.router.navigate(["/topfilms", film.ids.trakt]);
  }

  fetchNextPage(): void {
    this.currentPage += 1;
    this.getPopularFilms();
  }

  // fetchPreviousPage(): void {
  //   this.currentPage -= 1;
  //   this.getPopularFilms();
  // }
}
