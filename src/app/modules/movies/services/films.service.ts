import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { IFilm } from "../films";
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";
import { HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { LocalstorageService } from "../../shared/services/localstorage.service";

@Injectable({
  providedIn: "root"
})
export class FilmsService {
  constructor(
    private http: HttpClient,
    private _localstorageService: LocalstorageService
  ) {}

  // Headers to be added each time a call is made to trakt
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
      "trakt-api-version": "2"
    })
  };

  // Va chercher la liste des films populaires
  getPopularFilms(currentPage: number, limit: number = 15): Observable<any> {
    const options: any = {
      observe: "response",
      headers: this.httpOptions.headers
    };
    return this.http
      .get<IFilm[]>(
        `${
          environment.trakt.baseUrl
        }/movies/popular?page=${currentPage}&limit=${limit}`,
        options
      )
      .pipe(
        tap(console.log),
        map((response: HttpResponse<IFilm[]>) => {
          return {
            totalPageCount: parseInt(
              response.headers.get("x-pagination-page-count")
            ),
            currentPage: parseInt(response.headers.get("x-pagination-page")),
            movies: response.body
          };
        })
      );
  }

  // Constructs a request URL with the desired film's id
  // Server should respond with a single film rather than an array of films and give all the details we ask for
  getFilmsbyId(id: number): Observable<IFilm> {
    return this.http.get<IFilm>(
      `${environment.trakt.baseUrl}/movies/${id}?extended=full`,
      // extended=full: permet de récupérer l'entiéreté des détails d'une media, dans l'onglet summary
      this.httpOptions
    );
  }

  // Récupère tous les films vus d'un utilisateur
  getSeenMovies() {
    return this.http.get<IFilm[]>(
      `${environment.trakt.baseUrl}/users/${localStorage.getItem('slug')}/watched/movies`,
      this.httpOptions
    );
  }

  // Récupère tous les films dans la liste à voir d'un utilisateur
  getWishMovies(): Observable<IFilm[]> {
    return this.http.get<IFilm[]>(
      `${environment.trakt.baseUrl}/users/${localStorage.getItem('slug')}/watchlist/movies`,
      this.httpOptions
    );
  }

  // Ajoute un film à la liste des médias à voir de l'utilisateur
  addWishMovie(film: IFilm): Observable<IFilm[]> {
    const headers: HttpHeaders = this.httpOptions.headers.set(
      "Authorization",
      `Bearer ${
        JSON.parse(this._localstorageService.getItem("token")).access_token
      }`
    );
    this.httpOptions.headers = headers;
    return this.http.post<IFilm[]>(
      `${environment.trakt.baseUrl}/sync/watchlist`,
      {
        movies: [film]
      },
      this.httpOptions
    );
  }

  // Ajoute un film à la liste des médias vus par l'utilisateur
  addWatchedMovie(film: IFilm): Observable<IFilm[]> {
    const headers: HttpHeaders = this.httpOptions.headers.set(
      "Authorization",
      `Bearer ${
        JSON.parse(this._localstorageService.getItem("token")).access_token
      }`
    );
    this.httpOptions.headers = headers;
    return this.http.post<IFilm[]>(
      `${environment.trakt.baseUrl}/sync/history`,
      {
        films: [film]
      },
      this.httpOptions
    );
  }

  // Affiche les films similaires à un film, sur sa page de détails
  getRelatedMovies(id: number): Observable<IFilm[]> {
    return this.http.get<IFilm[]>(
      `${environment.trakt.baseUrl}/movies/${id}/related`,
      this.httpOptions
    );
  }

  // Récupére les images des films sur tmdb
  getMovieImage(id: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/movie/${id}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }

  // Récupère les backdrops des films sur tmdb
  getMovieBackdrop(id: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/movie/${id}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }


  
}
