import { Component, OnInit } from "@angular/core";
import { SearchService } from "../../../shared/services/search.service";
import { Router } from "@angular/router";
import { IFilm } from "../../../movies/films";
import { IShow } from "../../../shows/shows";

@Component({
  selector: "app-searchresult",
  templateUrl: "./searchresult.component.html",
  styleUrls: ["./searchresult.component.css"]
})
export class SearchresultComponent implements OnInit {
  searchTerm: string;
  films: IFilm[] = [];
  shows: IShow[] = [];
  // myMedias: any[] = [];
  results;

  displayMovies = false;
  displayShows = false;
  displayMedias = false;

  currentPage = 1;
  totalPage: number;
  hasNext = true;
  limit = 3;

  constructor(private _searchService: SearchService, private router: Router) {}

  ngOnInit() {}

  searchMovies() {
    this._searchService
      .searchMovies(this.searchTerm, this.currentPage, this.limit)
      .subscribe(response => {
        this.totalPage = response.totalPage;
        this.films = [...this.films, ...response.movies];

        this.hasNext = response.currentPage < response.totalPageCount;
        this.displayMovies = true;
        this.displayShows = false;
        this.displayMedias = false;
      });
  }

  searchShows() {
    this._searchService
      .searchShows(this.searchTerm, this.currentPage, this.limit)
      .subscribe(response => {
        this.totalPage = response.totalPage;
        this.shows = [...this.shows, ...response.tvshows];

        this.hasNext = response.currentPage < response.totalPageCount;
        this.displayShows = true;
        this.displayMovies = false;
        this.displayMedias = false;
      });
  }

  // searchBoth() {
  //   this._searchService
  //     .searchBoth(this.searchTerm, this.currentPage, this.limit)
  //     .subscribe(response => {
  //       this.totalPage = response.totalPage;
  //       this.myMedias = [...this.myMedias, ...response.medias];

  //       this.hasNext = response.currentPage < response.totalPageCount;
  //       this.displayMedias = true;
  //       this.displayShows = false;
  //       this.displayMovies = false;
  //     });
  // }

  // Navigue vers les pages de détails des films ou des séries sélectionnés dans la liste de résultat
  selectFilm(film) {
    this.router.navigate(["/search/movie", film.movie.ids.trakt]);
  }

  selectShow(show) {
    this.router.navigate(["/search/show", show.show.ids.trakt]);
  }

  // Pagination vers le bas
  fetchNextMovies(): void {
    this.currentPage += 1;
    this.searchMovies();
  }

  fetchNextShows(): void {
    this.currentPage += 1;
    this.searchShows();
  }
  // fetchNextMedias(): void {
  //   this.currentPage += 1;
  //   this.searchShows();
  // }
}
