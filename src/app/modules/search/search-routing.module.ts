import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SearchresultComponent } from "./pages";
import { DetailfilmComponent } from "../movies/pages";
import { DetailshowComponent } from "../shows/pages";

const routes: Routes = [
  { path: "", component: SearchresultComponent },
  { path: "movie/:ids.trakt", component: DetailfilmComponent },
  { path: "show/:ids.trakt", component: DetailshowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {}
