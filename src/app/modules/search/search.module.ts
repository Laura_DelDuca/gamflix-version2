import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SearchRoutingModule } from "./search-routing.module";
import { pages } from "./pages";
import { SharedModule } from "../shared/shared.module";
import { FormsModule } from "@angular/forms";
import { MoviesModule } from "../movies/movies.module";
import { ShowsModule } from "../shows/shows.module";

@NgModule({
  declarations: [...pages],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    FormsModule,
    MoviesModule,
    ShowsModule
  ]
})
export class SearchModule {}
