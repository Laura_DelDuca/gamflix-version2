export interface IActor     {
  name: string,
  ids: {
    trakt: number,
    slug: string,
    imdb: string,
    tmdb: number
  },
  biography: string,
  birthday: string,
  death: null,
  birthplace: string,
  homepage: string
}