import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetSlugComponent } from './get-slug.component';

describe('GetSlugComponent', () => {
  let component: GetSlugComponent;
  let fixture: ComponentFixture<GetSlugComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetSlugComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetSlugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
