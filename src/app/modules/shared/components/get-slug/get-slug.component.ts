import { Component, OnInit, OnChanges } from "@angular/core";
import { OauthService } from "src/app/modules/authorize/oauth.service";
import { UserService } from "src/app/modules/shared/services/user.service";

@Component({
  selector: "app-get-slug",
  templateUrl: "./get-slug.component.html",
  styleUrls: ["./get-slug.component.css"]
})
export class GetSlugComponent implements OnInit, OnChanges {
  public user;

  constructor(private _userService: UserService) {}

  ngOnInit() {
    this._userService.getUserInfo().subscribe(res => {
      this.user = res;
      localStorage.setItem("slug", this.user.user.ids.slug);
      console.log(this.user);
      console.log(localStorage.getItem("slug"));
    });
  }
  ngOnChanges() {
    this._userService.getUserInfo().subscribe(res => {
      this.user = res;
      localStorage.setItem("slug", this.user.user.ids.slug);
      console.log(localStorage.getItem("slug"));
    });
  }
}
