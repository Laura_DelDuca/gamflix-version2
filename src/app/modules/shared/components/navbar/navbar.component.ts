import { Component, OnInit } from "@angular/core";
import { OauthService } from "src/app/modules/authorize/oauth.service";
import { Router } from "@angular/router";
import { FilmsService } from "src/app/modules/movies/services/films.service";
import { UserService } from "../../services/user.service";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  isLogged;
  info;
  slug;
  storage = localStorage.getItem("token");
  constructor(
    private _oauthService: OauthService,
    private router: Router,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._oauthService.isLoggedIn$.subscribe(res => (this.isLogged = res));
    // console.log(localStorage);
    // console.log(this.isLogged);
    this.checkStorage();
    // this.getUserInfo();
  }

  login() {
    window.location.href =
      "https://trakt.tv/oauth/authorize?response_type=code&client_id=80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a&redirect_uri=https://gamflix.netlify.com/authorize";
    this._oauthService.updateLog(true);
  }

  revokeToken(): void {
    this._oauthService.revokeToken().subscribe();
    localStorage.clear();
    this._oauthService.updateLog(false);
    console.log(localStorage); // is empty
    console.log(this.isLogged);
    this.router.navigate(["/topfilms"]);
  }

  checkStorage() {
    if (this.storage !== undefined && this.storage !== null) {
      this._oauthService.updateLog(true);
    } else {
      this._oauthService.updateLog(false);
    }
  }

  // getUserInfo() {
  //   if (this.storage !== undefined && this.storage !== null) {
  //     this._userService.getUserInfo().subscribe(res => {
  //       this.slug = res.user.ids.slug;
  //       console.log(this.slug);
  //       console.log("fuckingcomputerfuckingjob");
  //     });
  //   }
  // }
}
