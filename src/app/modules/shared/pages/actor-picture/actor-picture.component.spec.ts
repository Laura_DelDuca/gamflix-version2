import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ActorPictureComponent } from "./actor-picture.component";

describe("ActorPictureComponent", () => {
  let component: ActorPictureComponent;
  let fixture: ComponentFixture<ActorPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActorPictureComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActorPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
