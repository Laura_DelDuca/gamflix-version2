import { Component, OnInit, Input } from "@angular/core";
import { ActorsService } from "../../services/actors.service";

@Component({
  selector: "app-actor-picture",
  templateUrl: "./actor-picture.component.html",
  styleUrls: ["./actor-picture.component.css"]
})
export class ActorPictureComponent implements OnInit {
  images;
  id: number;
  path;
  picture;
  @Input() actor;

  constructor(private _actorsService: ActorsService) {}

  ngOnInit() {
    this.getActorImage(this.actor);
  }

  getActorImage(id) {
    this._actorsService.getActorImage(id).subscribe(image => {
      this.images = image;
      this.path = this.images.profiles[0].file_path;
      this.picture = `https://image.tmdb.org/t/p/w200${this.path}`;
    });
  }
}
