import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { FilmsService } from "src/app/modules/movies/services/films.service";
import { ShowsService } from "src/app/modules/shows/services/shows.service";

@Component({
  selector: "app-backdrop",
  templateUrl: "./backdrop.component.html",
  styleUrls: ["./backdrop.component.css"]
})
export class BackdropComponent implements OnInit {
  id: number;
  images;
  @Input() film;
  @Input() show;
  path;
  backdrop;
  @Input() filmId;

  constructor(
    private _filmsService: FilmsService,
    private _showsService: ShowsService
  ) {}

  ngOnChanges() {
    this.getMovieBackdrop(this.film);
    this.getShowBackdrop(this.show);
  }

  ngOnInit() {
    this.getMovieBackdrop(this.film);
    this.getShowBackdrop(this.show);
  }

  // méthode pour récupérer le backdrop, la fonction est déja définie dans le service
  getMovieBackdrop(id) {
    this._filmsService.getMovieBackdrop(id).subscribe(image => {
      this.images = image;
      this.path = this.images.backdrop_path;
      this.backdrop = `https://image.tmdb.org/t/p/w500${this.path}`;
      // console.log(this.images);
      // console.log(this.path);
    });
  }

  getShowBackdrop(id) {
    this._showsService.getShowBackdrop(id).subscribe(image => {
      this.images = image;
      this.path = this.images.backdrop_path;
      this.backdrop = `https://image.tmdb.org/t/p/w500${this.path}`;
      // console.log(this.images);
      // console.log(this.path);
    });
  }
}
