import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailactorComponent } from './detailactor.component';

describe('DetailactorComponent', () => {
  let component: DetailactorComponent;
  let fixture: ComponentFixture<DetailactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
