import { Component, OnInit, Input } from "@angular/core";
import { ActorsService } from "../../services/actors.service";
import { ParamMap, ActivatedRoute } from "@angular/router";


@Component({
  selector: "app-detailactor",
  templateUrl: "./detailactor.component.html",
  styleUrls: ["./detailactor.component.css"]
})
export class DetailactorComponent implements OnInit {
  actor: any;
  id: number;
  public movies;
  public shows;
  moviesCasts;
  showsCasts;
  displayMovies = false;
  displayShows = false;


  constructor(
    private _actorsService: ActorsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = parseInt(params.get("id"));
      // console.warn(this.id);

      this.getSomeoneDetails(this.id);
      this.getSomeoneMovies(this.id);
      this.getSomeoneShows(this.id);
    });
  }

  getSomeoneDetails(id): void {
    this._actorsService
      .getSomeoneDetails(this.id)
      .subscribe(actor => (this.actor = actor));
  }

  getSomeoneMovies(id): void {
    this._actorsService.getSomeoneMovies(id).subscribe(movie => {
      this.movies = movie;
      // console.log(this.movies);
      this.moviesCasts = this.movies.cast;
      // console.log(this.moviesCasts);
    });
  }

  getSomeoneShows(id): void {
    this._actorsService.getSomeoneShows(id).subscribe(show => {
      this.shows = show;
      // console.log(this.shows);
      this.showsCasts = this.shows.cast;
      // console.log(this.showsCasts);
    });
  }

  seeMovies() {
    this.displayMovies = true;
    this.displayShows = false;
  }

  seeShows() {
    this.displayShows = true;
    this.displayMovies = false;
  }
}
