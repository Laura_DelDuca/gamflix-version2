import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({
  name: "convertToYears"
})
export class ConvertToYearsPipe implements PipeTransform {
  // Essai de transformer la date de naissance des acteurs en âge...
  transform(value: Date): string {
    let today = moment();
    let birthdate = moment(value);
    let years = today.diff(birthdate, "years");
    let html: string = years + " yr ";

    html += today.subtract(years, "years").diff(birthdate, "months") + " mo";

    return html;
  }
}
