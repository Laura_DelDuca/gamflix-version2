import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { FilmsService } from "src/app/modules/movies/services/films.service";
import { ShowsService } from "src/app/modules/shows/services/shows.service";

@Component({
  selector: "app-poster",
  templateUrl: "./poster.component.html",
  styleUrls: ["./poster.component.css"]
})
export class PosterComponent implements OnInit {
  id: number;
  images;
  @Input() film;
  @Input() show;
  path;
  poster;
  @Input() filmId;
  @Input() moviesCast;

  constructor(
    private _filmsService: FilmsService,
    private _showsService: ShowsService
  ) {}

  ngOnChanges() {
    this.getMovieImage(this.film);
    this.getShowImage(this.show);
  }

  ngOnInit() {
    this.getMovieImage(this.film);
    this.getShowImage(this.show);
  }

  getMovieImage(id) {
    this._filmsService.getMovieImage(id).subscribe(image => {
      this.images = image;
      this.path = this.images.poster_path;
      this.poster = `https://image.tmdb.org/t/p/w200${this.path}`;
      // console.log(this.film);
      // console.log(this.images);
      // console.log(this.path);
    });
  }

  getShowImage(id) {
    this._showsService.getShowImage(id).subscribe(image => {
      this.images = image;
      this.path = this.images.poster_path;
      this.poster = `https://image.tmdb.org/t/p/w200${this.path}`;
      // console.log(this.images);
      // console.log(this.path);
    });
  }
}
