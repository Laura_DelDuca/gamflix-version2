import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  OnDestroy
} from "@angular/core";

@Component({
  selector: "app-trailer",
  templateUrl: "./trailer.component.html",
  styleUrls: ["./trailer.component.css"]
})
export class TrailerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() trailerUrl: string;
  // @Input() movieId;
  private _videoId: string;
  private _YT: any;
  private _player: any;
  video;

  constructor() {}

  ngOnChanges() {
    // console.log(this._videoId);
    this._videoId = this._extractVideoId();
    this._resetPlayer();
    if (this._YT) {
      this.createPlayer();
    }
    // console.log(this._videoId);
    // console.log("trailer component onChange");
  }

  ngOnInit() {
    this._loadYoutubeScript();
  }

  ngAfterViewInit(): void {
    if (!this._YT) {
      this._YT = window["YT"];
    }

    if (!this._player) {
      this.createPlayer();
    }
  }

  ngOnDestroy() {
    console.log("destroy trailer component");
  }

  private _extractVideoId() {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = this.trailerUrl.match(regExp);
    if (match && match[7].length == 11) {
      return match[7];
    } else {
      alert("Could not extract video ID.");
    }
  }

  private _loadYoutubeScript(): void {
    if (!document.getElementById("youtube_script")) {
      var tag = document.createElement("script");

      tag.src = "https://www.youtube.com/iframe_api";
      tag.id = "youtube_script";

      var firstScriptTag = document.getElementsByTagName("script")[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      window["onYouTubeIframeAPIReady"] = e => {
        this._YT = window["YT"];
        if (!this._player) {
          this.createPlayer();
        }
      };
    }
  }

  private createPlayer() {
    if (this._YT) {
      this._player = new this._YT.Player("player", {
        videoId: this._videoId
      });
    }
  }

  private _resetPlayer() {
    this._player = null;
  }
}
