import { Injectable } from "@angular/core";
import { HttpHeaders, HttpSentEvent } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { IActor } from "../actors";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ActorsService {
  constructor(private http: HttpClient) {}

  // Headers to be added each time a call is made to trakt
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
      "trakt-api-version": "2"
    })
  };

  // Va rechercher la liste des acteurs au casting d'un film
  getCastingMovie(id: number): Observable<IActor[]> {
    return this.http.get<IActor[]>(
      `${environment.trakt.baseUrl}/movies/${id}/people`,
      this.httpOptions
    );
  }

  // Va rechercher la liste des acteurs au casting d'une série
  getCastingShow(id: number): Observable<IActor[]> {
    return this.http.get<IActor[]>(
      `${environment.trakt.baseUrl}/shows/${id}/people`,
      this.httpOptions
    );
  }

  // Va rechercher les photos des acteurs
  getActorImage(id: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/person/${id}/images?api_key=f8ec3cbf9b0fc508b7b5f00125e19293`
    );
  }

  // Va rechercher les infos des acteurs
  getSomeoneDetails(id: number) {
    return this.http.get(
      `${environment.trakt.baseUrl}/people/${id}?extended=full`,
      this.httpOptions
    );
  }

  // Va rechercher la liste des films dans lesquels un acteur a joué
  getSomeoneMovies(id: number) {
    return this.http.get(
      `${environment.trakt.baseUrl}/people/${id}/movies?extended=full`,
      this.httpOptions
    );
  }

  getSomeoneShows(id:number) {
    return this.http.get(
      `${environment.trakt.baseUrl}/people/${id}/shows?extended=full`,
      this.httpOptions
    );
  }
}
