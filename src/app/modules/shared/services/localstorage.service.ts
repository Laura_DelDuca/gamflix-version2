import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LocalstorageService {
  private storage: Map<string, any>;

  constructor() {
    this.storage = new Map<string, any>();
  }

  setItem(key: string, value: any): void {
    this.storage.set(key, value);
    window.localStorage.setItem(key, value);
  }

  getItem(key: string): any {
    let value: any = this.storage.get(key);
    if (value) {
      return value;
    }

    value = window.localStorage.getItem(key);

    if (value) {
      this.storage.set(key, value);
    }

    return value;
  }
}
