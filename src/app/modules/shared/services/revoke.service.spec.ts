import { TestBed } from '@angular/core/testing';

import { RevokeService } from './revoke.service';

describe('RevokeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RevokeService = TestBed.get(RevokeService);
    expect(service).toBeTruthy();
  });
});
