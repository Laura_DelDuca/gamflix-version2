import { Injectable } from "@angular/core";
import { LocalstorageService } from "./localstorage.service";

@Injectable({
  providedIn: "root"
})
export class TokenLocalStorageService extends LocalstorageService {
  constructor() {
    super();
  }

  getAccessToken(): string {
    const value: string = this.getItem("token") || "";
    let token: any;
    let accessToken: string;

    if (value.length) {
      token = JSON.parse(value);
      accessToken = token.access_token;
    }

    return accessToken;
  }
}
