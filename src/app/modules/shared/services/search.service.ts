import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { IFilm } from "../../movies/films";
import { environment } from "src/environments/environment";
import { HttpHeaders, HttpSentEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { IShow } from "../../shows/shows";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class SearchService {
  constructor(private http: HttpClient, private router: Router) {}

  // Headers to be added each time a call is made to trakt
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
      "trakt-api-version": "2"
    })
  };

  searchMovies(
    searchTerm: string,
    currentPage: number,
    limit: number = 5
  ): Observable<any> {
    const options: any = {
      observe: "response",
      headers: this.httpOptions.headers
    };
    return this.http
      .get<IFilm[]>(
        `${
          environment.trakt.baseUrl
        }/search/movie?query=${searchTerm}&page=${currentPage}&limit=${limit}`,
        options
      )
      .pipe(
        map((response: HttpResponse<IFilm[]>) => {
          return {
            totalPageCount: parseInt(
              response.headers.get("x-pagination-page-count")
            ),
            currentPage: parseInt(response.headers.get("x-pagination-page")),
            movies: response.body
          };
        })
      );
  }

  searchShows(
    searchTerm: string,
    currentPage: number,
    limit: number = 5
  ): Observable<any> {
    const options: any = {
      observe: "response",
      headers: this.httpOptions.headers
    };
    return this.http
      .get<IShow[]>(
        `${
          environment.trakt.baseUrl
        }/search/show?query=${searchTerm}&page=${currentPage}&limit=${limit}`,
        options
      )
      .pipe(
        map((response: HttpResponse<IShow[]>) => {
          return {
            totalPageCount: parseInt(
              response.headers.get("x-pagination-page-count")
            ),
            currentPage: parseInt(response.headers.get("x-pagination-page")),
            tvshows: response.body
          };
        })
      );
  }

  searchBoth(
    searchTerm: string,
    currentPage: number,
    limit: number = 5
  ): Observable<any> {
    const options: any = {
      observe: "response",
      headers: this.httpOptions.headers
    };
    return this.http
      .get<any[]>(
        `${
          environment.trakt.baseUrl
        }/search/movie,show?query=${searchTerm}&page=${currentPage}&limit=${limit}`,
        options
      )
      .pipe(
        map((response: HttpResponse<any[]>) => {
          return {
            totalPageCount: parseInt(
              response.headers.get("x-pagination-page-count")
            ),
            currentPage: parseInt(response.headers.get("x-pagination-page")),
            medias: response.body
          };
        })
      );
  }
}
