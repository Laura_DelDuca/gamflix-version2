import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { LocalstorageService } from "./localstorage.service";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(
    private http: HttpClient,
    private _localstorageService: LocalstorageService
  ) {}

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-version": "2",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a"      
    })
  };

  // Récupère les informations de l'utilisateur, dont son id
  getUserInfo() {
    const headers: HttpHeaders = this.httpOptions.headers.set(
      "Authorization",
      `Bearer ${
        JSON.parse(this._localstorageService.getItem("token")).access_token
      }`
    );
    this.httpOptions.headers = headers;

    return this.http.get<any>(
      `${environment.trakt.baseUrl}/users/settings`,
      this.httpOptions
    );
  }
}
