import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DetailactorComponent } from "./pages/detailactor/detailactor.component";

const routes: Routes = [{ path: ":id", component: DetailactorComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule {}
