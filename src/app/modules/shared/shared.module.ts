import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedRoutingModule } from "./shared-routing.module";
import { PosterComponent } from "./pages/poster/poster.component";
import { BackdropComponent } from "./pages/backdrop/backdrop.component";
import { ActorPictureComponent } from "./pages/actor-picture/actor-picture.component";
import { TrailerComponent } from './pages/trailer/trailer.component';
import { DetailactorComponent } from './pages/detailactor/detailactor.component';
import { GetSlugComponent } from './components/get-slug/get-slug.component';

@NgModule({
  declarations: [
    PosterComponent,
    BackdropComponent,
    ActorPictureComponent,
    TrailerComponent,
    DetailactorComponent,
    GetSlugComponent
  ],
  imports: [CommonModule, SharedRoutingModule],

  exports: [PosterComponent, BackdropComponent, ActorPictureComponent, TrailerComponent, DetailactorComponent, GetSlugComponent]
})
export class SharedModule {}
