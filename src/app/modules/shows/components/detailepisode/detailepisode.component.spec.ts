import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailepisodeComponent } from './detailepisode.component';

describe('DetailepisodeComponent', () => {
  let component: DetailepisodeComponent;
  let fixture: ComponentFixture<DetailepisodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailepisodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailepisodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
