import { Component, OnInit, Input } from "@angular/core";
import { IEpisode } from "../../episodes";

@Component({
  selector: "app-detailepisode",
  templateUrl: "./detailepisode.component.html",
  styleUrls: ["./detailepisode.component.css"]
})
export class DetailepisodeComponent implements OnInit {
  @Input() episode: IEpisode;

  constructor() {}

  ngOnInit() {
    // console.log(this.seasonId);
  }
}
