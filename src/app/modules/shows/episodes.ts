export interface IEpisode {
    season: number,
    number: number,
    title: string,
    ids: {
      trakt: number,
      tvdb: number,
      imdb: string,
      tmdb: number
    },
    number_abs: null,
    overview: string,
    first_aired: string,
    updated_at: string,
    rating: number,
    votes: number,
    comment_count: number,
    available_translations: string,
    runtime: number
  }