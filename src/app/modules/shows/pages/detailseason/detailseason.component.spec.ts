import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { DetailseasonComponent } from "./detailseason.component";

describe("DetailseasonComponent", () => {
  let component: DetailseasonComponent;
  let fixture: ComponentFixture<DetailseasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailseasonComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailseasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
