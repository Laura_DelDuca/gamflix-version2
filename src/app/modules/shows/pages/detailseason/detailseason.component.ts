import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { ShowsService } from "../../services/shows.service";
import { IEpisode } from "../../episodes";
import { IShow } from "src/app/modules/shows/shows";
import { ISeason } from "src/app/modules/shows/seasons";


@Component({
  selector: "app-detailseason",
  templateUrl: "./detailseason.component.html",
  styleUrls: ["./detailseason.component.css"]
})
export class DetailseasonComponent implements OnInit {
  public seasonId;
  public showId;
  episodes;
  public selectedEpisode: IEpisode;
  displayDetails = false;
  episodeNumber;
  episodeId;
  @Input() show: IShow;
  @Input() season: ISeason;
  tmdbId;
  displayArrow = true;
  showTitle;

  constructor(
    private route: ActivatedRoute,
    private _showService: ShowsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get("number"));
      this.seasonId = id;

      let number = parseInt(params.get("ids.trakt"));
      this.showId = number;

      this.getEpisodes(this.seasonId);
      this._showService.getShowsbyId(this.showId).subscribe(res => {
        this.show = res;
        // Récupération de l'id TMDB de la série pour pouvoir créer la requête pour recevoir les images des épisodes
        this.tmdbId = this.show.ids.tmdb;
        this.showTitle = this.show.title;
        // console.log(this.show);
        // console.log(this.tmdbId);
        // console.log(this.showTitle);
      });
    });
  }

  getEpisodes(id): void {
    this._showService
      .getEpisodes(this.seasonId, this.showId)
      .subscribe(episode => {
        this.episodes = episode;
        console.log(episode);
        // essai de récupérer l'id tmdb de l'épisode dans cette fonction
        // this.episodeNumber = this.episodes.ids.tmdb;
      });
  }

  // fonction au clic sur le titre d'un épisode, sélectionne l'épisode et affiche ses détails
  selectEpisode(episode: IEpisode) {
    this.selectedEpisode = episode;
    // Au chargement des titres d'épisodes, les détails ne s'affichent pas,
    // au clic, ils apparaissent grâce à displayDetails qui passe à true
    this.displayDetails = true;
  }

  selectPreviousSeason() {
    let previousId = this.seasonId - 1;
    this.router.navigate([`topshows/${this.showId}/season`, previousId]);
    // if (this.seasonId > 2) {
    //   this.displayArrow = true;
    // } else {
    //   this.displayArrow = false;
    // }
  }

  selectNextSeason() {
    let nextId = this.seasonId + 1;
    this.router.navigate([`topshows/${this.showId}/season`, nextId]);
  }
}
