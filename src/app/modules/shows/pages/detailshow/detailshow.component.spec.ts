import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailshowComponent } from './detailshow.component';

describe('DetailshowComponent', () => {
  let component: DetailshowComponent;
  let fixture: ComponentFixture<DetailshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
