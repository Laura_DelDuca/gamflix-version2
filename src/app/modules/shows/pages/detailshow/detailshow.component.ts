import { Component, OnInit } from "@angular/core";
import { ShowsService } from "../../services/shows.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { IShow } from "../../shows";
import { ISeason } from "../../seasons";
import { Router } from "@angular/router";
import { ActorsService } from "../../../shared/services/actors.service";
import { IActor } from "../../../shared/actors";

@Component({
  selector: "app-detailshow",
  templateUrl: "./detailshow.component.html",
  styleUrls: ["./detailshow.component.css"]
})
export class DetailshowComponent implements OnInit {
  // Route Parameter (tmbd) to get a show's details
  public showId;
  show: IShow;
  // Tableau de toutes les saisons d'une série à afficher
  seasons: ISeason[] = [];
  public similars;
  actors: IActor[];
  people;
  id: number;
  trailer: string;
  shows: IShow[];
  private _traktIds: number[];
  inWishlist: boolean = true;

  seenshows: IShow[];
  private _traktSeenIds: number[];
  inWatchedlist: boolean = true;

  constructor(
    private _showsService: ShowsService,
    private route: ActivatedRoute,
    private router: Router,
    private _actorsService: ActorsService
  ) {}

  ngOnInit(): void {
    // Allows to pass the route parameter: the tmdb id
    // We subscribe to the method defined in the service
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get("ids.trakt"));
      this.showId = id;
      this.trailer = null;
      this.getShowsbyId(this.showId);
      this.getSeasons(this.showId);
      this.getRelatedShows(id);
      this.getCastingShow(id);
      this.getWishShows();
      this.getSeenShows();
    });
  }

  // We subscribe to the getShowsbyId method defined in the service
  getShowsbyId(id): void {
    this._showsService.getShowsbyId(this.showId).subscribe(show => {
      this.show = show;
      this.trailer = this.show.trailer;
    });
  }

  getSeasons(id): void {
    this._showsService.getSeasons(this.showId).subscribe(season => {
      this.seasons = season;
    });
  }

  onSelect(season) {
    this.router.navigate([`/topshows/${this.showId}/season`, season.number]);
  }

  addWishShow(show: IShow) {
    this._showsService.addWishShow(show).subscribe(() => this.getWishShows());
  }

  addWatchedShow(show: IShow) {
    this._showsService
      .addWatchedShow(show)
      .subscribe(() => this.getSeenShows());
  }

  getRelatedShows(id) {
    this._showsService.getRelatedShows(id).subscribe(similar => {
      this.similars = similar;
    });
  }

  selectShow(show) {
    this.router.navigate(["/topshows", show.ids.trakt]);
  }

  getCastingShow(id) {
    this._actorsService.getCastingShow(id).subscribe(actor => {
      this.people = actor;
      this.actors = this.people.cast;
      // console.log(this.actors);
    });
  }

  selectActor(actor) {
    this.router.navigate(["/actor", actor.ids.trakt]);
  }

  getWishShows() {
    this._showsService.getWishShows().subscribe(data => {
      this.shows = data;
      // console.log(this.shows);
      this._traktIds = this.shows.map((item: any) => item.show.ids.trakt);
      this.isShowIn();
    });
  }

  isShowIn() {
    this.inWishlist = this._traktIds.includes(this.showId);
  }

  getSeenShows() {
    this._showsService.getSeenShows().subscribe(data => {
      this.seenshows = data;
      console.log(this.seenshows);
      this._traktSeenIds = this.seenshows.map(
        (item: any) => item.show.ids.trakt
      );
      console.log(this._traktSeenIds);
      this.isShowSeen();
    });
  }

  isShowSeen() {
    this.inWatchedlist = this._traktSeenIds.includes(this.showId);
    console.log(this.inWatchedlist);
  }
}
