import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodeImagesComponent } from './episode-images.component';

describe('EpisodeImagesComponent', () => {
  let component: EpisodeImagesComponent;
  let fixture: ComponentFixture<EpisodeImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpisodeImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodeImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
