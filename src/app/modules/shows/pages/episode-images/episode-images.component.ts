import { Component, OnInit, Input } from "@angular/core";
import { ShowsService } from "../../services/shows.service";

@Component({
  selector: "app-episode-images",
  templateUrl: "./episode-images.component.html",
  styleUrls: ["./episode-images.component.css"]
})
export class EpisodeImagesComponent implements OnInit {
  constructor(private _showsService: ShowsService) {}
  images;
  path;
  picture;
  @Input() showNumber;
  @Input() seasonNumber;
  @Input() episodeNumber;

  ngOnInit() {
    console.log("test");
    this.getEpisodesImages(
      this.showNumber,
      this.seasonNumber,
      this.episodeNumber
    );
  }

  getEpisodesImages(id, season, episode) {
    // console.log("test");
    this._showsService
      .getEpisodesImages(id, season, episode)
      .subscribe(image => {
        this.images = image;
        // console.log(image);
        this.path = this.images.still_path;
        this.picture = `https://image.tmdb.org/t/p/w200${this.path}`;
        // console.warn(this.path);
      });
  }
}
