import { TopshowsComponent } from "./topshows/topshows.component";
import { DetailshowComponent } from "./detailshow/detailshow.component";
import { DetailseasonComponent } from "./detailseason/detailseason.component";
import { SeasonImagesComponent } from "./season-images/season-images.component";
import { EpisodeImagesComponent } from "./episode-images/episode-images.component";

export const pages: any[] = [
  TopshowsComponent,
  DetailshowComponent,
  DetailseasonComponent,
  SeasonImagesComponent,
  EpisodeImagesComponent
];

export * from "./topshows/topshows.component";
export * from "./detailshow/detailshow.component";
export * from "./detailseason/detailseason.component";
export * from "./season-images/season-images.component";
export * from "./episode-images/episode-images.component";
