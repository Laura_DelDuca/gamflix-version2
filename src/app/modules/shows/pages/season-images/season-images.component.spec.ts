import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonImagesComponent } from './season-images.component';

describe('SeasonImagesComponent', () => {
  let component: SeasonImagesComponent;
  let fixture: ComponentFixture<SeasonImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
