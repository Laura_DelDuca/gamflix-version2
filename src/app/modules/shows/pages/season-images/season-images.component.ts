import { Component, OnInit, Input } from "@angular/core";
import { ShowsService } from "../../services/shows.service";

@Component({
  selector: "app-season-images",
  templateUrl: "./season-images.component.html",
  styleUrls: ["./season-images.component.css"]
})
export class SeasonImagesComponent implements OnInit {
  images;
  path;
  picture;
  @Input() season;
  @Input() show;

  constructor(private _showsService: ShowsService) {}

  ngOnInit() {
    this.getSeasonsImages(this.show, this.season);
  }

  getSeasonsImages(id, season) {
    this._showsService.getSeasonsImages(id, season).subscribe(image => {
      this.images = image;
      this.path = this.images.poster_path;
      this.picture = `https://image.tmdb.org/t/p/w200${this.path}`;
      // console.log(this.images);
      // console.log(this.path);
    });
  }
}
