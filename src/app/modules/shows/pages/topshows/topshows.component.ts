import { Component, OnInit } from "@angular/core";
import { ShowsService } from "../../services/shows.service";
import { Router } from "@angular/router";
import { IShow } from "../../shows";

@Component({
  selector: "app-topshows",
  templateUrl: "./topshows.component.html",
  styleUrls: ["./topshows.component.css"]
})
export class TopshowsComponent implements OnInit {
  shows: IShow[] = [];
  currentPage = 1;
  totalPage: number;
  hasNext = true;
  limit = 10;

  constructor(private _showsService: ShowsService, private router: Router) {}

  ngOnInit() {
    this.getPopularShows();
  }

  getPopularShows() {
    this._showsService
      .getPopularShows(this.currentPage, this.limit)
      .subscribe(response => {
        this.totalPage = response.totalPage;
        this.shows = [...this.shows, ...response.shows];

        this.hasNext = response.currentPage < response.totalPageCount;
      });
  }

  onSelect(show) {
    this.router.navigate(["/topshows", show.ids.trakt]);
  }

  fetchNextPage(): void {
    this.currentPage += 1;
    this.getPopularShows();
  }
}
