export interface ISeason {
  number: number;
  ids: {
    trakt: number;
    tvdb: number;
    tmdb: number;
  };
  rating: number;
  votes: number;
  episode_count: number;
  aired_episodes: number;
  title: string;
  overview: null;
  first_aired: string;
  network: string;
}
