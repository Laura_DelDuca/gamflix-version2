import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { IShow } from "../shows";
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";
import { HttpHeaders } from "@angular/common/http";
// Fichier dans lequel se trouve l'URL de base, auquel il faut ajouter les / pour les requêtes
import { environment } from "src/environments/environment";
import { ISeason } from "../seasons";
import { IEpisode } from "../episodes";
import { LocalstorageService } from "../../shared/services/localstorage.service";

@Injectable({
  providedIn: "root"
})
export class ShowsService {
  constructor(
    private http: HttpClient,
    private _localstorageService: LocalstorageService
  ) {}

  // Headers to be added each time a call is made to trakt
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "trakt-api-key":
        "80c967d895b99974099fa560f3b32d9e773a01fdc450416f64c34fae81ff211a",
      "trakt-api-version": "2"
    })
  };

  // Va rechercher la liste des séries populaires et renvoie un tableau
  getPopularShows(currentPage: number, limit: number = 15): Observable<any> {
    const options: any = {
      observe: "response",
      headers: this.httpOptions.headers
    };
    return this.http
      .get<IShow[]>(
        `${
          environment.trakt.baseUrl
        }/shows/popular?page=${currentPage}&limit=${limit}`,
        options
      )
      .pipe(
        tap(console.log),
        map((response: HttpResponse<IShow[]>) => {
          return {
            totalPageCount: parseInt(
              response.headers.get("x-pagination-page-count")
            ),
            currentPage: parseInt(response.headers.get("x-pagination-page")),
            shows: response.body
          };
        })
      );
  }

  // Constructs a request URL with the desired film's id
  // Server should respond with a single film rather than an array of films and give all the details we ask for
  getShowsbyId(id: number): Observable<IShow> {
    return this.http.get<IShow>(
      `${environment.trakt.baseUrl}/shows/${id}?extended=full`,
      // extended=full: permet de récupérer l'entiéreté des détails d'une media, dans l'onglet summary de l'API
      this.httpOptions
    );
  }

  // Va rechercher toutes les saisons existantes pour chaque série, renvoie un tableau
  getSeasons(id: number): Observable<ISeason[]> {
    return this.http.get<ISeason[]>(
      `${environment.trakt.baseUrl}/shows/${id}/seasons?extended=full`,
      this.httpOptions
    );
  }

  // Va chercher tous les épisodes pour une saison en particulier aisi que les détails de l'épisode
  getEpisodes(id: number, season: number): Observable<IEpisode[]> {
    return this.http.get<IEpisode[]>(
      `${
        environment.trakt.baseUrl
      }/shows/${season}/seasons/${id}?extended=full`,
      this.httpOptions
    );
  }

  // Récupère toutes les séries vues d'un utilisateur
  getSeenShows(): Observable<IShow[]> {
    return this.http.get<IShow[]>(
      // extended=noseasons: it won't return season or episode info
      `${environment.trakt.baseUrl}/users/${localStorage.getItem(
        "slug"
      )}/watched/shows?extended=noseasons`,
      this.httpOptions
    );
  }

  // Récupère toutes les séries dans la liste à voir d'un utilisateur
  getWishShows(): Observable<IShow[]> {
    return this.http.get<IShow[]>(
      `${environment.trakt.baseUrl}/users/${localStorage.getItem(
        "slug"
      )}/watchlist/shows`,
      this.httpOptions
    );
  }

  // Ajoute une série à la liste des médias à voir de l'utilisateur
  addWishShow(show: IShow): Observable<IShow[]> {
    const headers: HttpHeaders = this.httpOptions.headers.set(
      "Authorization",
      `Bearer ${
        JSON.parse(this._localstorageService.getItem("token")).access_token
      }`
    );
    this.httpOptions.headers = headers;
    return this.http.post<IShow[]>(
      `${environment.trakt.baseUrl}/sync/watchlist`,
      {
        shows: [show]
      },
      this.httpOptions
    );
  }

  // Ajoute une série à la liste des médias vus par l'utilisateur
  addWatchedShow(show: IShow): Observable<IShow[]> {
    const headers: HttpHeaders = this.httpOptions.headers.set(
      "Authorization",
      `Bearer ${
        JSON.parse(this._localstorageService.getItem("token")).access_token
      }`
    );
    this.httpOptions.headers = headers;
    return this.http.post<IShow[]>(
      `${environment.trakt.baseUrl}/sync/history`,
      {
        shows: [show]
      },
      this.httpOptions
    );
  }

  // Récupère les séries similaires à une série
  getRelatedShows(id: number): Observable<IShow[]> {
    return this.http.get<IShow[]>(
      `${environment.trakt.baseUrl}/shows/${id}/related`,
      this.httpOptions
    );
  }

  // Récupérer le poster des séries sur tmdb
  getShowImage(id: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/tv/${id}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }

  // Récupère le backdrop d'une série sur tmdb
  getShowBackdrop(id: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/tv/${id}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }

  // Récupère les images pour les différentes saisons d'une série
  getSeasonsImages(id: number, season: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/tv/${id}/season/${season}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }

  // Récupère les images des épisodes pour chaque saison de chaque série
  getEpisodesImages(id: number, season: number, episode: number) {
    return this.http.get(
      `https://api.themoviedb.org/3/tv/${id}/season/${season}/episode/${episode}?api_key=f8ec3cbf9b0fc508b7b5f00125e19293&language=en-US`
    );
  }

  // Récupération du trailer d'une série
}
