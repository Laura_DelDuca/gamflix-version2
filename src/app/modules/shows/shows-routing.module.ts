import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  TopshowsComponent,
  DetailshowComponent,
  DetailseasonComponent
} from "./pages";

const routes: Routes = [
  {
    path: "",
    component: TopshowsComponent
  },
  {
    path: ":ids.trakt",
    component: DetailshowComponent
  },
  {
    path: ":ids.trakt/season/:number",
    component: DetailseasonComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShowsRoutingModule {}
