import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ShowsRoutingModule } from "./shows-routing.module";
import { pages } from "./pages";
import { components } from "./components";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [...pages, ...components],
  imports: [CommonModule, ShowsRoutingModule, SharedModule]
})
export class ShowsModule {}
