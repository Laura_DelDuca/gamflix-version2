import { SeenfilmsComponent } from "./seenfilms/seenfilms.component";
import { SeenshowsComponent } from "./seenshows/seenshows.component";

export const components: any[] = [SeenfilmsComponent, SeenshowsComponent];

export * from "./seenfilms/seenfilms.component";
export * from "./seenshows/seenshows.component";
