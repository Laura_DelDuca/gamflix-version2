import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeenfilmsComponent } from './seenfilms.component';

describe('SeenfilmsComponent', () => {
  let component: SeenfilmsComponent;
  let fixture: ComponentFixture<SeenfilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeenfilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeenfilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
