import { Component, OnInit, Input } from "@angular/core";
import { FilmsService } from "../../../movies/services/films.service";
import { IFilm } from "../../../movies/films";
import { Router } from '@angular/router';

@Component({
  selector: "app-seenfilms",
  templateUrl: "./seenfilms.component.html",
  styleUrls: ["./seenfilms.component.css"]
})
export class SeenfilmsComponent implements OnInit {
  films: IFilm[];

  constructor(private _filmsService: FilmsService, private router: Router) {}

  ngOnInit() {
    this.getSeenMovies();
  }

  getSeenMovies() {
    this._filmsService.getSeenMovies().subscribe(data => (this.films = data));
  }

  selectFilm(film) {
    this.router.navigate(["/watched/movie", film.movie.ids.trakt]);
  }
}
