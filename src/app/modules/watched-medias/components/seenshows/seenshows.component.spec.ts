import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeenshowsComponent } from './seenshows.component';

describe('SeenshowsComponent', () => {
  let component: SeenshowsComponent;
  let fixture: ComponentFixture<SeenshowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeenshowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeenshowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
