import { Component, OnInit } from "@angular/core";
import { ShowsService } from "../../../shows/services/shows.service";
import { IShow } from "../../../shows/shows";
import { Router } from "@angular/router";

@Component({
  selector: "app-seenshows",
  templateUrl: "./seenshows.component.html",
  styleUrls: ["./seenshows.component.css"]
})
export class SeenshowsComponent implements OnInit {
  shows: IShow[];

  constructor(private _showsService: ShowsService, private router: Router) {}

  ngOnInit() {
    this.getSeenShows();
  }

  getSeenShows() {
    this._showsService.getSeenShows().subscribe(data => (this.shows = data));
  }

  selectShow(show) {
    this.router.navigate(["/watched/show", show.show.ids.trakt]);
  }
}
