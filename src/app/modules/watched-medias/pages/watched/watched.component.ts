import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-watched",
  templateUrl: "./watched.component.html",
  styleUrls: ["./watched.component.css"]
})
export class WatchedComponent implements OnInit {
  displayMovies = true;
  displayShows = false;

  constructor() {}

  ngOnInit() {}

  seenMovies() {
    this.displayMovies = true;
    this.displayShows = false;
  }

  seenShows() {
    this.displayShows = true;
    this.displayMovies = false;
  }
}
