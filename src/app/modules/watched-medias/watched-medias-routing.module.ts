import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { WatchedComponent } from "./pages";
import { DetailfilmComponent } from '../movies/pages';
import { DetailshowComponent } from '../shows/pages';
import { AuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: WatchedComponent,
    canActivate: [AuthGuard]
  },
  {path : "movie/:ids.trakt", component: DetailfilmComponent, canActivate: [AuthGuard]},
  {path: "show/:ids.trakt", component: DetailshowComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchedMediasRoutingModule {}
