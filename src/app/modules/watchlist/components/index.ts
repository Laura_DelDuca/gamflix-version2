import { NextfilmsComponent } from "./nextfilms/nextfilms.component";
import { NextshowsComponent } from "./nextshows/nextshows.component";

export const components: any[] = [NextfilmsComponent, NextshowsComponent];

export * from "./nextfilms/nextfilms.component";
export * from "./nextshows/nextshows.component";
