import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextfilmsComponent } from './nextfilms.component';

describe('NextfilmsComponent', () => {
  let component: NextfilmsComponent;
  let fixture: ComponentFixture<NextfilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextfilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextfilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
