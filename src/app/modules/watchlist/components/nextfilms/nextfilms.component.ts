import { Component, OnInit } from "@angular/core";
import { FilmsService } from "../../../movies/services/films.service";
import { IFilm } from "../../../movies/films";
import { Router } from "@angular/router";

@Component({
  selector: "app-nextfilms",
  templateUrl: "./nextfilms.component.html",
  styleUrls: ["./nextfilms.component.css"]
})
export class NextfilmsComponent implements OnInit {
  films: IFilm[];

  constructor(private _filmsService: FilmsService, private router: Router) {}

  ngOnInit() {
    this.getWishMovies();
  }

  getWishMovies() {
    this._filmsService.getWishMovies().subscribe(data => (this.films = data));
  }

  selectFilm(film) {
    this.router.navigate(["/wishlist/movie", film.movie.ids.trakt]);
  }
}
