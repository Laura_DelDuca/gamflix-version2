import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextshowsComponent } from './nextshows.component';

describe('NextshowsComponent', () => {
  let component: NextshowsComponent;
  let fixture: ComponentFixture<NextshowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextshowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextshowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
