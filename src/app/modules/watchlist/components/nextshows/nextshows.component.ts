import { Component, OnInit } from "@angular/core";
import { ShowsService } from "../../../shows/services/shows.service";
import { IShow } from "../../../shows/shows";
import { Router } from "@angular/router";

@Component({
  selector: "app-nextshows",
  templateUrl: "./nextshows.component.html",
  styleUrls: ["./nextshows.component.css"]
})
export class NextshowsComponent implements OnInit {
  shows: IShow[];

  constructor(private _showsService: ShowsService, private router: Router) {}

  ngOnInit() {
    this.getWishShows();
  }

  getWishShows() {
    this._showsService.getWishShows().subscribe(data => (this.shows = data));
  }

  selectShow(show) {
    this.router.navigate(["/wishlist/show", show.show.ids.trakt]);
  }
}
