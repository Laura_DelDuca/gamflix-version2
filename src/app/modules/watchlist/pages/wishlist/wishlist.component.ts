import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-wishlist",
  templateUrl: "./wishlist.component.html",
  styleUrls: ["./wishlist.component.css"]
})
export class WishlistComponent implements OnInit {
  displayMovies = true;
  displayShows = false;

  constructor() {}

  ngOnInit() {}

  nextMovies() {
    this.displayMovies = true;
    this.displayShows = false;
  }

  nextShows() {
    this.displayShows = true;
    this.displayMovies = false;
  }
}
