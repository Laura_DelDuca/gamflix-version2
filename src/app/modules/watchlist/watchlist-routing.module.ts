import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { WishlistComponent } from "./pages";
import { DetailfilmComponent } from "../movies/pages";
import { DetailshowComponent } from "../shows/pages";
import { AuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: WishlistComponent,
    canActivate: [AuthGuard]
  },
  { path: "movie/:ids.trakt", component: DetailfilmComponent },
  { path: "show/:ids.trakt", component: DetailshowComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchlistRoutingModule {}
