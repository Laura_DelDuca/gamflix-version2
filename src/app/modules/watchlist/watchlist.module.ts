import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { WatchlistRoutingModule } from "./watchlist-routing.module";
import { pages } from "./pages";
import { components } from "./components";
import { SharedModule } from "../shared/shared.module";
import { MoviesModule } from '../movies/movies.module';
import { ShowsModule } from '../shows/shows.module';

@NgModule({
  declarations: [...pages, ...components],
  imports: [CommonModule, WatchlistRoutingModule, SharedModule, MoviesModule, ShowsModule]
})
export class WatchlistModule {}
