export const environment = {
  production: false,
  trakt: {
    baseUrl: "https://api.trakt.tv",
    redirectUri: "https://gamflix.netlify.com"
  }
};
